# Rationale 

Bulk scaling images in multiple sub-directories might be useful, for example when uploading a lot of images to Woocommerce store.

This script allow us to do it quickly without compiling Imagemagick and all its dependencies, which might be quite challenging.

# Prerequisites

Docker

## Features 

* Scale all jpgs into squares by adding white background
* Recognize which dimension is bigger and set both dimensions to the value of grater one
* Goes into first level sub-directories
* Recreates sub-directories in the output folder

## Usage

* Clone the repository 
* Enter the project's root directory
* Create folder called 'input' in the project's 
* Copy all files you want to convert, they might be in sub-folders
* Run this command in project's root dir

<pre>
<code>
docker run -it -v $PWD:/img/src inight1997/image-magick /bin/sh /img/src/resize.sh
</code>
</pre>

It will download a Docker image and execute a bash script.

You may want to adjust file permissions in the 'output' folder. For Debian based linux distros it could be done with this command:

<pre>
<code>
sudo chown -R $USER:$USER output
</code>
</pre>
