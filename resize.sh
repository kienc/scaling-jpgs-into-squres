#!/bin/sh
cd /img/src/input
function count_jpgs {
extensions="jpg"
for ext in $extensions; do
 find . -maxdepth 1 -iname "*.$ext" -print0 | tr -d -c "\000" | wc -c
done
}

function width() {
  identify -format "%[fx:w]" "$i" 
                    }
function height() {
  identify -format "%[fx:h]" "$i" 
                    }

 jpgs=$(count_jpgs)

 if [ ${jpgs} != 0 ]; then
                   echo "Processing ${jpgs} files in main directory" 
                   mkdir -p  /img/src/output/
                   for i in *.jpg; do 
                    width=$(width)
                    height=$(height)
                      if [ ${width} > ${height} ]; then
                      convert "$i" -resize ${width}x${width} -gravity center -background white -extent ${width}x${width} "/img/src/output/${i%.jpg}.jpg"
                      else
                      convert "$i" -resize ${height}x${height} -gravity center -background white -extent ${height}x${height} "/img/src/output/${i%.jpg}.jpg"
                      fi
                     echo "Processing ${i}" 
                   done   
            else
                      echo "no .img files in main dir"
          fi
  
for D in *; do
    if [ -d "${D}" ]; then
        echo "Entering ${D} directory"
        mkdir -p  /img/src/output/${D}
        cd ${D} 
        jpgs=$(count_jpgs)
        if [ ${jpgs} != 0 ]; then
                   echo "Processing ${jpgs} files in this directory" 
          for i in *.jpg; do
                width=$(width)
                height=$(height)
                  if [ ${width} > ${height} ]; then
                  convert "$i" -resize ${width}x${width} -gravity center -background white -extent ${width}x${width} "/img/src/output/${D}/${i%.jpg}.jpg"
                  else
                  convert "$i" -resize ${height}x${height} -gravity center -background white -extent ${height}x${height} "/img/src/output/${D}/${i%.jpg}.jpg"
                        fi
                     echo "Processing ${i}" 
                   done   
            else
                      echo "no .img files in this dir"
          fi
          cd ..  
    fi
 done
